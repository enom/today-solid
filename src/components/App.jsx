import Calendar from './Calendar.jsx'
import Clock from './Clock.jsx'
import Weather from './Weather.jsx'

/**
 * @typedef {import('solid-js').JSXElement} JSXElement
 */

/**
 * Main application component.
 *
 * @returns {JSXElement}
 */
export default function App () {
  return (
    <div class='grid' style='grid-template-columns: 38.2% 61.8%'>
      <div>
        <Clock />

        <hr class='border-0 my-1' />

        <Weather />
      </div>

      <div class='-mt-3'>
        <Calendar />
      </div>
    </div>
  )
}
