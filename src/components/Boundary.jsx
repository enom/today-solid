import { ErrorBoundary } from 'solid-js'

/**
 * @typedef {import('solid-js').JSXElement} JSXElement
 */

/**
 * @typedef {import('solid-js').ParentProps} ParentProps
 */

/**
 * Boundary component combining ErrorBoundary and Suspense.
 *
 * @param {ParentProps} props
 * @returns {JSXElement}
 */
export default function Boundary (props) {
  return (
    <ErrorBoundary
      fallback={(err) => (
        <div class='mt-5 text-center'>
          <span class='bg-red-600 text-black px-1 pb-0.5'>{err.toString()}</span>
        </div>
      )}
    >
      {props.children}
    </ErrorBoundary>
  )
}
