import { createComputed, createSignal, on, Show } from 'solid-js'
import { For } from 'solid-js/web'

import Loading from './Loading.jsx'
import * as en from '--/utils/en.js'
import { useCalendar, MAX_RESULTS } from '--/data/calendar.jsx'
import { useSync } from '--/data/sync.jsx'

/**
 * @typedef {import('solid-js').JSXElement} JSXElement
 */

/**
 * Calendar component.
 *
 * @returns {JSXElement}
 */
export default function Calendar () {
  // Loading our events API results
  const [events] = useCalendar()

  // Loading sync to update the filter dates every hour
  const [{ hour }] = useSync()

  // Filter dates
  const [today, setToday] = createSignal()
  const [tomorrow, setTomorrow] = createSignal()
  const [someday, setSomeday] = createSignal()

  /**
   * Helper function that creates a date object set to midnight.
   *
   * @param {Number} [offset=0] Offset in days
   * @returns {Date}
   */
  const midnight = (offset = 0) => {
    const date = new Date()

    date.setDate(date.getDate() + offset)
    date.setHours(0, 0, 0)

    return date
  }

  /**
   * Reactive function returning today's events.
   *
   * @returns {Array<CalendarEvent>}
   */
  const todays = () => events().filter((event) => {
    return event.date > today() && event.date < tomorrow()
  })

  /**
   * Reactive function returning tomorrow's events.
   *
   * @returns {Array<CalendarEvent>}
   */
  const tomorrows = () => events().filter((event) => {
    return event.date > tomorrow() && event.date < someday()
  })

  /**
   * Reactive function returning all remaining(n-2) events.
   *
   * @returns {Array<CalendarEvent>}
   */
  const somedays = () => events().filter((event) => {
    return event.date > someday()
  }).slice(0, MAX_RESULTS - 2)

  // Computation on sync{ hour }
  createComputed(on(hour, () => {
    // Skip sync initialization
    if (hour() === undefined) return

    // Update filter dates every hour
    setToday(midnight())
    setTomorrow(midnight(1))
    setSomeday(midnight(2))
  }))

  return (
    <Show when={events()} fallback={<Loading>Loading calendar...</Loading>}>
      <div class='grid' style='grid-template-columns: min-content min-content auto'>
        <List events={todays()} title='Today' date={today()} />
        <List events={tomorrows()} title='Tomorrow' date={tomorrow()} />
        <List events={somedays()} title='Someday' />
      </div>
    </Show>
  )
}

/**
 * Properties for the List component.
 *
 * @typedef {Object} ListProps
 * @property {Date} [date] List date
 * @property {Array} events List of events
 * @property {string} title List title
 */

/**
 * Calendar event listing component.
 *
 * @param {ListProps} props Component properties
 * @returns {JSX.Element}
 */
function List (props) {
  // Helper functions to convert dates into English text
  const day = (date) => en.date(date, { weekday: 'short' })
  const month = (date) => en.date(date, { month: 'short' })
  const hour = (date) => en.time(date, { hour: '2-digit', hour12: false })
  const minute = (date) => en.time(date, { minute: '2-digit' })

  return (
    <>
      <h2 class='col-span-3 mb-1 mt-3 text-xl'>
        {props.title}
        {props.date && (
          <small> / {day(props.date)} {month(props.date)} {props.date.getDate()}</small>
        )}
      </h2>

      <For each={props.events} fallback={<p class='col-span-3 ml-3'>No events</p>}>
        {(entry) => (
          <>
            <Show when={props.date === undefined}>
              <div class='ml-3'>
                {month(entry.date)}
              </div>

              <div class='ml-1 text-right'>
                {entry.date.getDate()}{en.suffix(entry.date.getDate())}
              </div>

              <div class='ml-3'>{entry.name}</div>
            </Show>

            <Show when={props.date !== undefined}>
              <div class='col-span-2 ml-3 text-right'>
                {hour(entry.date)}h{minute(entry.date)}
              </div>

              <div class='ml-3'>{entry.name}</div>
            </Show>
          </>
        )}
      </For>
    </>
  )
}
