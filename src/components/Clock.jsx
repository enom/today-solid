import { createComputed, createSignal, on } from 'solid-js'

import * as en from '--/utils/en.js'
import { useSync } from '--/data/sync.jsx'

/**
 * @typedef {import('solid-js').JSXElement} JSXElement
 */

/**
 * Clock component.
 *
 * @returns {JSXElement}
 */
export default function Clock () {
  // Load sync signal which updates our clock every minute
  const [{ minute }] = useSync()

  // Local clock state
  const [digits, setDigits] = createSignal({})
  const [labels, setLabels] = createSignal({})

  // Computation on sync{ minute }
  createComputed(on(minute, () => {
    // Displaying the current time
    const now = new Date()

    // Update clock state every minute
    setDigits({
      minute: en.time(now, { minute: '2-digit' }),
      hour: en.time(now, { hour: '2-digit', hour12: false }),
      day: en.date(now, { day: '2-digit' }),
      month: en.date(now, { month: '2-digit' })
    })

    setLabels({
      day: en.date(now, { weekday: 'long' }),
      month: en.date(now, { month: 'long' })
    })
  }))

  return (
    <div class='grid grid-rows-3 text-xl' style='grid-template-columns: auto min-content auto;'>
      <div class='text-right text-3xl'>{digits().hour}</div>
      <div class='px-1 text-3xl'>:</div>
      <div class='text-3xl'>{digits().minute}</div>
      <div class='text-right'>{labels().day}</div>
      <div class='row-span-2 self-center px-1 -mt-3'>/</div>
      <div class='pl-2'>{digits().month}</div>
      <div class='text-right pr-2'>{digits().day}</div>
      <div>{labels().month}</div>
    </div>
  )
}
