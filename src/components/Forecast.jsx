import { For } from 'solid-js'

import pad from '--/utils/pad.jsx'
import { useSync } from '--/data/sync.jsx'
import { useWeather } from '--/data/weather.jsx'

/**
 * @typedef {import('solid-js').JSXElement} JSXElement
 */

// Safe degree symbol character entity
const DEG = String.fromCharCode(176)

/**
 * Forecast component.
 *
 * @returns {JSXElement}
 */
export default function Weather () {
  const [{ forecast }] = useWeather()
  const [{ hour }] = useSync()

  return (
    <For each={forecast()}>
      {(data, i) => (
        <>
          <div class='text-right'>{pad((hour() + i()) % 24)}h00</div>
          <div class='text-right' title='actual temperature'>{data.temp + DEG}</div>
          <div>/</div>
          <div title='apparent temperature'>
            {data.feel < 0 ? '' : <span class='invisible'>-</span>}{data.feel + DEG}
          </div>
          <div>{data.text ?? data.pop + '%'}</div>
        </>
      )}
    </For>
  )
}
