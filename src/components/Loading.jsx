/**
 * @typedef {import('solid-js').JSXElement} JSXElement
 */

/**
 * @typedef {import('solid-js').ParentProps} ParentProps
 */

/**
 * @typedef {Object} LoadingProps
 * @property {string} [class]
 */

/**
 * Loading placeholder component.
 *
 * @param {LoadingProps & ParentProps} props
 * @returns {JSXElement}
 */
export default function Loading (props) {
  return (
    <p class={`mt-5 ${props.class ?? ''}`}>{props.children}</p>
  )
}
