import { ErrorBoundary, Show, Suspense } from 'solid-js'

import Loading from '--/components/Loading.jsx'
import Repeat from '--/utils/Repeat.jsx'
import pad from '--/utils/pad.jsx'
import { useMeteo } from '--/data/meteo.jsx'
import { useSync } from '--/data/sync.jsx'

/**
 * @typedef {import('solid-js').JSXElement} JSXElement
 */

// Safe degree symbol character entity
const DEG = String.fromCharCode(176)

/**
 * Open Meteo component.
 *
 * @returns {JSXElement}
 */
export default function Meteo () {
  const [{ forecast, precipitation }] = useMeteo()
  const [{ hour }] = useSync()

  /**
   *
   * @param {{ hours: 12 | 24, right?: boolean }} props
   * @returns {JSX.Element}
   */
  const Precipitation = (props) => {
    const hours = () => precipitation().hours < props.hours ? precipitation().hours : props.hours
    const type = () => precipitation()[props.hours].snow ? 'snow' : 'rain'
    const text = () => hours() + 'h ' + type() + 'fall'
    const total = () => precipitation()[props.hours][type()] + (type() === 'snow' ? 'cm' : 'mm')

    return (
      <Show
        when={props.right}
        fallback={(
          <>
            <div class='pr-3'>{total()}</div>
            <div>{text()}</div>
          </>
        )}
      >
        <div class='text-right'>{text()}</div>
        <div class='text-right pl-3'>{total()}</div>
      </Show>
    )
  }

  return (
    <>
      <ErrorBoundary
        fallback={(err) => (
          <Loading class='text-center'>
            <span class='bg-red-600 text-black px-1'>{err.toString()}</span>
          </Loading>
        )}
      >
        <Suspense
          fallback={(
            <Loading class='text-center'>Loading Meteo...</Loading>
          )}
        >
          <div class='grid' style='grid-template-columns: 4fr 1fr max-content 1fr 4fr;'>
            <Repeat count={forecast().length}>
              {(i) => (
                <>
                  <div class='text-right'>{pad((hour() + i) % 24)}h00</div>
                  <div class='text-right' title='actual temperature'>{forecast()[i].temp + DEG}</div>
                  <div class='px-3'>/</div>
                  <div title='apparent temperature'>{forecast()[i].feel + DEG}</div>
                  <div>{i === 0 ? forecast()[i].text : forecast()[i].pop + '%'}</div>
                </>
              )}
            </Repeat>

            <Precipitation hours={12} right />
            <div class='px-3'>/</div>
            <Precipitation hours={24} />
          </div>
        </Suspense>
      </ErrorBoundary>
    </>
  )
}
