import { useWeather } from '--/data/weather.jsx'

/**
 * @typedef {import('solid-js').JSXElement} JSXElement
 */

/**
 * @typedef {import('--/data/weather.jsx').PrecipitationMemo} PrecipitationState
 */

/**
 * @typedef {Object} PartialProps
 * @property {12|24} hours The number of hours for the precipitation data.
 * @property {boolean} [right] Whether the text should be right-aligned.
 */

/**
 * Precipitation component.
 *
 * @returns {JSXElement}
 */
export default function Precipitation () {
  const [{ precipitation }] = useWeather()

  /**
   * Partial precipitation component.
   *
   * @returns {JSXElement}
   */
  const Partial = (props) => {
    const data = () => precipitation()?.[props.key] ?? {}
    const type = () => data().snow ? 'snow' : 'rain'
    const text = () => props.key.slice(1) + 'h ' + type() + 'fall'
    const unit = () => data().snow ? 'cm' : 'mm'
    const total = () => (data()[type()] ?? 0) + unit()

    return (
      <>
        <div class={props.right ? 'text-right' : ''}>
          {props.right ? text() : total()}
        </div>

        <div class={props.right ? 'text-right' : ''}>
          {props.right ? total() : text()}
        </div>
      </>
    )
  }

  return (
    <>
      <Partial key='h12' right />
      <div>/</div>
      <Partial key='h24' />
    </>
  )
}
