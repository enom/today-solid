import Forecast from './Forecast.jsx'
import Precipitation from './Precipitation.jsx'

/**
 * @typedef {import('solid-js').JSXElement} JSXElement
 */

/**
 * Weather component.
 *
 * @returns {JSXElement}
 */
export default function Weather () {
  return (
    <div class='grid gap-x-3' style='grid-template-columns: 4fr 1fr max-content 1fr 4fr;'>
      <Forecast />

      <Precipitation />
    </div>
  )
}
