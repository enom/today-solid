import { createComputed, createContext, createSignal, on, useContext } from 'solid-js'

import { useSync } from './sync.jsx'

/**
 * @typedef {import('solid-js').Accessor<T>} Accessor
 * @template T
 */

/**
 * @typedef {import('solid-js').Signal<T>} Signal
 * @template T
 */

/**
 * @typedef {Object} CalendarEvent
 * @property {String} name Calendar event name
 * @property {Date} date Calendar event date
 */

/**
 * @typedef {Accessor<CalendarEvent[]>} CalendarState
 */

/**
 * @typedef {Object} CalendarProviderProps
 * @property {String} key Google API key
 * @property {String} client Google API client
 */

// Create calendar context which will hold all upcoming events for 45 days
const CalendarContext = createContext()

// Number of events to fetch
export const MAX_RESULTS = 10

/**
 * Calendar context provider.
 *
 * @param {CalendarProviderProps & ParentProps} props Component properties
 * @returns  {JSXElement}
 */
export default function CalendarProvider (props) {
  // Google API
  let gapi

  // Use our global intervals instead of creating local ones
  const [{ hour, second }] = useSync()

  // Calendar provides a simple array of events
  /** @type {Signal<CalendarEvent[]>} */
  const [state, setState] = createSignal()

  // Google client config
  const client = {
    apiKey: props.key,
    clientId: props.client,
    discoveryDocs: ['https://www.googleapis.com/discovery/v1/apis/calendar/v3/rest'],
    scope: 'https://www.googleapis.com/auth/calendar.readonly'
  }

  // Google calendar request config
  const calendar = {
    calendarId: 'primary',
    timeMin: (new Date()).toISOString(),
    showDeleted: false,
    singleEvents: true,
    maxResults: MAX_RESULTS,
    orderBy: 'startTime'
  }

  /**
   * Access Google's authentication.
   *
   * @returns {Object}
   */
  const auth = () => gapi.auth2.getAuthInstance()

  /**
   * Check if user is authenticated.
   *
   * @returns {Boolean}
   */
  const authed = () => auth().isSignedIn

  /**
   * Main Google initialization function.
   *
   * @returns {void}
   */
  const init = () => {
    // Only initialize once
    gapi = window.gapi

    // Load the required Google authentication before fetching events
    gapi.load('client:auth2', () => {
      // Initialize our Google API client which isn't a promise
      gapi.client.init(client).then(request)
    })
  }

  /**
   * Main calendar request function that lists upcoming events.
   *
   * @returns {void}
   */
  const request = () => {
    // Skip possible call before Google finished loading
    if (gapi.auth2 === undefined) return

    // Allow cookie authentication
    if (authed().get()) {
      // Attempt to fetch the list of calendar events
      gapi.client.calendar.events.list(calendar).then((response) => {
        // Update our calendar events
        setState(response.result.items.map((event) => {
          return { date: new Date(event.start.dateTime), name: event.summary }
        }))
      })
    } else {
      // Listen to the signed in state and fire a request on success
      authed().listen((success) => success && request())

      // Show login screen
      auth().signIn()
    }
  }

  // Computation on sync{ second } signal
  createComputed(on(second, () => {
    // Check if Google's API is loaded every second and fire a request when it is
    if (window.gapi && gapi === undefined) init()
  }))

  // Computation on sync{ hour }
  createComputed(on(hour, () => {
    // Check if our timers and Google API are initialized before sending a request
    if (hour() !== undefined && gapi !== undefined) request()
  }))

  return (
    <CalendarContext.Provider value={[state]}>
      {props.children}
    </CalendarContext.Provider>
  )
}

/**
 * Calendar context accessor.
 *
 * @returns {[CalendarState]}
 */
export function useCalendar () {
  return useContext(CalendarContext)
}
