import CalendarProvider from './calendar.jsx'
import SyncProvider from './sync.jsx'
import WeatherProvider from './weather.jsx'

/**
 * @typedef {import('solid-js').JSXElement} JSXElement
 */

/**
 *  @typedef {import('solid-js').ParentProps} ParentProps
 */

/**
 * Store provider that wraps children with all our providers.
 *
 * @param {ParentProps} props Component properties
 * @returns {JSXElement}
 */
export default function Provider (props) {
  return (
    <SyncProvider>
      <WeatherProvider
        key={import.meta.env.VITE_WEATHER_KEY}
        query={import.meta.env.VITE_WEATHER_QUERY}
      >
        <CalendarProvider
          client={import.meta.env.VITE_GOOGLE_ID}
          key={import.meta.env.VITE_GOOGLE_KEY}
          secret={import.meta.env.VITE_GOOGLE_SECRET}
        >
          {props.children}
        </CalendarProvider>
      </WeatherProvider>
    </SyncProvider>
  )
}
