/* eslint-env browser */
import { createContext, createEffect, createMemo, createResource, createSignal, useContext } from 'solid-js'

import now from '--/utils/now.jsx'
import { useSync } from './sync.jsx'

/**
 * @typedef {import('solid-js').Accessor<T>} Accessor
 * @template T
 */

/**
 * @typedef {import('solid-js').JSXElement} JSXElement
 */

/**
 * @typedef {import('solid-js').ParentProps} ParentProps
 */

/**
 * @typedef {import('solid-js').Resource<T>} Resource
 * @template T
 */

/**
 * @typedef {import('solid-js').Signal<T>} Signal
 * @template T
 */

/**
 * @typedef {Object} Accumulated
 * @property {number} rain
 * @property {number} snow
 */

/**
 * @typedef {Object} Forecast
 * @property {string} feel Apparent temperature
 * @property {string} temp Actual temperature
 * @property {number} [pop] Probability of precipitation
 * @property {string} [text] Weather description
 */

/**
 * @typedef {Object} History
 * @property {string} timestamp
 * @property {string} type
 * @property {number} precipitation
 */

/**
 * @typedef {Object} Meteo
 * @property {Object} current
 * @property {number} current.temperature_2m
 * @property {number} current.apparent_temperature
 * @property {number} current.rain
 * @property {number} current.showers
 * @property {number} current.snowfall
 * @property {number} current.weather_code
 * @property {Object} hourly
 * @property {string[]} hourly.time
 * @property {number[]} hourly.temperature_2m
 * @property {number[]} hourly.apparent_temperature
 * @property {number[]} hourly.precipitation_probability
 */

/**
 * @typedef {Object} MeteoProviderProps
 * @property {number} lat
 * @property {number} lon
 */

/**
 * @typedef {Object} MeteoState
 * @property {Accessor<Forecast[]>} forecast
 * @property {Resource<Meteo>} meteo
 * @property {Accessor<History[]>} history
 * @property {Accessor<Precipitation>} precipitation
 */

/**
 * @typedef {{ 12: Accumulated, 24: Accumulated, hours: number }} Precipitation
 */

/**
 * Local storage key for precipitation history.
 */
const KEY_STORAGE = 'today.solid/meteo.precipitation'

/**
 * Number of hourly weather forecasts to track.
 */
const MAX_HOURLY = 10

/**
 * Number of snowfall entries to track.
 */
const MAX_HISTORY = 24

/**
 * WMO weather codes.
 */
const WEATHER_CODES = {
  0: 'Clear sky',
  1: 'Mainly clear',
  2: 'Partly cloudy',
  3: 'Overcast',
  45: 'Fog',
  48: 'Depositing rime fog',
  51: 'Light drizzle',
  53: 'Moderate drizzle',
  55: 'Dense drizzle',
  56: 'Light freezing drizzle',
  57: 'Dense freezing drizzle',
  61: 'Slight rain',
  63: 'Moderate rain',
  65: 'Heavy rain',
  66: 'Light freezing rain',
  67: 'Heavy freezing rain',
  71: 'Slight snow fall',
  73: 'Moderate snow fall',
  75: 'Heavy snow fall',
  77: 'Snow grains',
  80: 'Slight rain showers',
  81: 'Moderate rain showers',
  82: 'Violent rain showers',
  85: 'Slight snow showers',
  86: 'Heavy snow showers',
  95: 'Slight or moderate thunderstorm',
  96: 'Thunderstorm with slight hail',
  99: 'Thunderstorm with heavy hail'
}

const MeteoContext = createContext()

/**
 * Open Meteo data provider component.
 *
 * @param {MeteoProviderProps & ParentProps} props
 * @returns {JSXElement} The MeteoContext provider with weather data.
 */
export default function MeteoProvider (props) {
  const url = 'https://api.open-meteo.com/v1/forecast'
  const params = new URLSearchParams({
    latitude: props.lat,
    longitude: props.lon,
    timezone: 'auto',
    forecast_hours: 10,
    // forecast_minutely_15: 4,
    current: ['temperature_2m', 'apparent_temperature', 'rain', 'showers', 'snowfall', 'weather_code'],
    hourly: ['temperature_2m', 'apparent_temperature', 'precipitation_probability']
    // minutely_15: ['temperature_2m', 'apparent_temperature', 'weather_code']
  })

  const [{ hour }] = useSync()

  /** @type {Signal<History[]>} */
  const [history, setHistory] = createSignal(load() ?? [])
  /** @type {Resource<Meteo>} */
  const [meteo] = createResource(() => hour(), async () => {
    const response = await fetch(`${url}?${params}`)
    /** @type {Meteo} */
    const data = await response.json()

    if (!response.ok) throw new Error(`${response.status} ${data.reason}`)

    setHistory((history) => {
      const { dd, hh, mm, y } = now()
      const time = `${y}-${mm}-${dd}T${hh}:00`

      for (let i = 0; i < history.length; i++) {
        if (history[i][0] === time) return history
      }

      const type = data.current.snowfall ? 'snow' : 'rain'
      const fall = data.current.snowfall || data.current.rain + data.current.showers

      return [[time, type, fall]].concat(history).slice(0, MAX_HISTORY)
    })

    return data
  })

  const forecast = createMemo(() => {
    const data = meteo()

    if (!data || data.error) return []

    const {
      time,
      temperature_2m: temp,
      apparent_temperature: feel,
      precipitation_probability: pop
    } = data.hourly
    const results = [{
      feel: data.current.apparent_temperature.toFixed(),
      temp: data.current.temperature_2m.toFixed(),
      text: WEATHER_CODES[data.current.weather_code]
    }]

    for (let i = 0; i < time.length; i++) {
      const length = results.push({
        feel: feel[i].toFixed(),
        pop: pop[i],
        temp: temp[i].toFixed()
      })

      if (length >= MAX_HOURLY) break
    }

    return results
  })

  const precipitation = createMemo(() => {
    const data = history()
    const results = {
      12: { rain: 0, snow: 0 },
      24: { rain: 0, snow: 0 },
      hours: data.length
    }

    for (let i = 0; i < data.length; i++) {
      const [, type, fall] = data[i]

      if (i < 12) results[12][type] += fall

      results[24][type] += fall
    }

    return results
  })

  const state = { forecast, history, meteo, precipitation }

  // Save precipitation history changes to local storage
  createEffect(() => save(history()))

  return (
    <MeteoContext.Provider value={[state]}>
      {props.children}
    </MeteoContext.Provider>
  )
}

/**
 * Save precipitation history to local storage.
 *
 * @param {History[]} data
 */
function save (data) {
  try {
    if (Array.isArray(data) && data.length) {
      localStorage.setItem(KEY_STORAGE, JSON.stringify(data))
    }
  } catch (e) {
    alert('failed to save weather history to local storage')

    console.error(e)
  }
}

/**
 * Load precipitation history from local storage.
 *
 * @returns {History[]}
 */
function load () {
  try {
    return JSON.parse(localStorage.getItem(KEY_STORAGE))
  } catch (e) {
    // ignore errors loading
  }
}

/**
 * Meteo data context accessor.
 *
 * @returns {[MeteoState]}
 */
export function useMeteo () {
  return useContext(MeteoContext)
}
