import { createContext, createSignal, onCleanup, useContext } from 'solid-js'

import now from '--/utils/now.jsx'

/**
 * @typedef {import('solid-js').JSXElement} JSXElement
 */

/**
 * @typedef {import('solid-js').Accessor<T>} Accessor
 * @template T
 */

/**
 * @typedef {import('solid-js').ParentProps} ParentProps
 */

/**
 * @typedef {Object} SyncContextState
 * @property {Accessor<number>} hour Current hour
 * @property {Accessor<number>} minute Current minute
 * @property {Accessor<number>} second Current second
 */

// Create sync context with signals for seconds, minutes, and hours
const SyncContext = createContext()

/**
 * Sync timer context provider.
 *
 * @param {ParentProps} props Component properties
 * @returns {JSXElement}
 */
export default function SyncProvider (props) {
  const { h, i, s } = now()

  // Sync signals
  const [hour, setHour] = createSignal(h)
  const [minute, setMinute] = createSignal(i)
  const [second, setSecond] = createSignal(s)

  // State object passed to provider
  const state = { hour, minute, second }

  /**
   * Main interval function used to create synchronizable timers.
   *
   * @returns {void}
   */
  const tick = () => {
    const { h, i, s } = now()

    // Tick is once every second so we can always update that
    setSecond(s)

    // Only update minutes when it changes to prevent unnecessary reativity
    if (i !== minute()) setMinute(i)

    // Same goes for hours which handles API calls
    if (h !== hour()) setHour(h)
  }

  // Tick every second for clock
  const interval = setInterval(tick, 1000)

  // Being clean for the sake of it
  onCleanup(() => clearInterval(interval))

  return (
    <SyncContext.Provider value={[state]}>
      {props.children}
    </SyncContext.Provider>
  )
}

/**
 * Sync timer context accessor.
 *
 * @returns {Array<SyncContextState>}}
 */
export function useSync () {
  return useContext(SyncContext)
}
