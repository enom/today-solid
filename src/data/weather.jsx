/* eslint-env browser */

import { createContext, createMemo, createResource, useContext } from 'solid-js'

// ---

import isoDateTime, { isoDate } from '--/utils/iso.jsx'
import { useSync } from './sync.jsx'

// ---

/**
 * @typedef {import('solid-js').Accessor<T>} Accessor
 * @template T
 */

/**
 * @typedef {import('solid-js').JSXElement} JSXElement
 */

/**
 * @typedef {import('solid-js').ParentProps} ParentProps
 */

/**
 * @typedef {import('solid-js').Resource<T>} Resource
 * @template T
 */

/**
 * @typedef {import('solid-js').ResourceReturn<T>} ResourceReturn
 * @template T
 */

/**
 * @typedef {import('solid-js/store').Store<T>} Store
 * @template T
 */

// ---

/**
 * @typedef {Object} Astro
 * @property {string} sunrise The sunrise time.
 * @property {string} sunset The sunset time.
 * @property {string} moonrise The moonrise time.
 * @property {string} moonset The moonset time.
 * @property {string} moon_phase The moon phase.
 * @property {number} moon_illumination The moon illumination percentage.
 * @property {number} is_moon_up Indicator if the moon is up (1) or not (0).
 * @property {number} is_sun_up Indicator if the sun is up (1) or not (0).
 */

/**
 * @typedef {Object} Condition
 * @property {string} text The weather condition text.
 * @property {string} icon The URL to the weather condition icon.
 * @property {number} code The weather condition code.
 */

/**
 * @typedef {Object} Current
 * @property {number} last_updated_epoch The last updated time in epoch format.
 * @property {string} last_updated The last updated time in human-readable format.
 * @property {number} temp_c The current temperature in Celsius.
 * @property {number} temp_f The current temperature in Fahrenheit.
 * @property {number} is_day Indicator if it is day (1) or night (0).
 * @property {Condition} condition The current weather condition.
 * @property {number} wind_mph The wind speed in miles per hour.
 * @property {number} wind_kph The wind speed in kilometers per hour.
 * @property {number} wind_degree The wind direction in degrees.
 * @property {string} wind_dir The wind direction as a string.
 * @property {number} pressure_mb The atmospheric pressure in millibars.
 * @property {number} pressure_in The atmospheric pressure in inches.
 * @property {number} precip_mm The precipitation amount in millimeters.
 * @property {number} precip_in The precipitation amount in inches.
 * @property {number} humidity The humidity percentage.
 * @property {number} cloud The cloud cover percentage.
 * @property {number} feelslike_c The feels-like temperature in Celsius.
 * @property {number} feelslike_f The feels-like temperature in Fahrenheit.
 * @property {number} windchill_c The wind chill temperature in Celsius.
 * @property {number} windchill_f The wind chill temperature in Fahrenheit.
 * @property {number} heatindex_c The heat index temperature in Celsius.
 * @property {number} heatindex_f The heat index temperature in Fahrenheit.
 * @property {number} dewpoint_c The dew point temperature in Celsius.
 * @property {number} dewpoint_f The dew point temperature in Fahrenheit.
 * @property {number} vis_km The visibility in kilometers.
 * @property {number} vis_miles The visibility in miles.
 * @property {number} uv The UV index.
 * @property {number} gust_mph The wind gust speed in miles per hour.
 * @property {number} gust_kph The wind gust speed in kilometers per hour.
 */

/**
 * @typedef {Object} Day
 * @property {number} maxtemp_c The maximum temperature in Celsius.
 * @property {number} maxtemp_f The maximum temperature in Fahrenheit.
 * @property {number} mintemp_c The minimum temperature in Celsius.
 * @property {number} mintemp_f The minimum temperature in Fahrenheit.
 * @property {number} avgtemp_c The average temperature in Celsius.
 * @property {number} avgtemp_f The average temperature in Fahrenheit.
 * @property {number} maxwind_mph The maximum wind speed in miles per hour.
 * @property {number} maxwind_kph The maximum wind speed in kilometers per hour.
 * @property {number} totalprecip_mm The total precipitation in millimeters.
 * @property {number} totalprecip_in The total precipitation in inches.
 * @property {number} totalsnow_cm The total snowfall in centimeters.
 * @property {number} avgvis_km The average visibility in kilometers.
 * @property {number} avgvis_miles The average visibility in miles.
 * @property {number} avghumidity The average humidity percentage.
 * @property {number} daily_will_it_rain Indicator if it will rain (1) or not (0).
 * @property {number} daily_chance_of_rain The chance of rain percentage.
 * @property {number} daily_will_it_snow Indicator if it will snow (1) or not (0).
 * @property {number} daily_chance_of_snow The chance of snow percentage.
 * @property {Condition} condition The weather condition for the day.
 * @property {number} uv The UV index.
 */

/**
 * @typedef {Object} Forecast
 * @property {ForecastDay[]} forecastday The forecast for multiple days.
 */

/**
 * @typedef {Object} ForecastResponse
 * @property {Location} location The location data.
 * @property {Current} current The current weather data.
 * @property {Forecast} forecast The weather forecast data.
 */

/**
 * @typedef {Object} HistoryResponse
 * @property {Location} location The location data.
 * @property {Forecast} forecast The weather forecast data.
 */

/**
 * @typedef {Object} ForecastDay
 * @property {string} date The date of the forecast.
 * @property {number} date_epoch The date in epoch format.
 * @property {Day} day The day forecast.
 * @property {Astro} astro The astronomical data.
 * @property {Hour[]} hour The hourly forecast.
 */

/**
 * @typedef {Object} ForecastMemo
 * @property {Number} feel Feels like temperature
 * @property {Number} pop Probability of precipitation
 * @property {Number} rain Rainfall in mm
 * @property {Number} snow Snowfall in mm
 * @property {Number} temp Temperature
 * @property {string} text Weather description
 */

/**
 * @typedef {Object} Hour
 * @property {number} time_epoch The time in epoch format.
 * @property {string} time The time in human-readable format.
 * @property {number} temp_c The temperature in Celsius.
 * @property {number} temp_f The temperature in Fahrenheit.
 * @property {number} is_day Indicator if it is day (1) or night (0).
 * @property {Condition} condition The weather condition.
 * @property {number} wind_mph The wind speed in miles per hour.
 * @property {number} wind_kph The wind speed in kilometers per hour.
 * @property {number} wind_degree The wind direction in degrees.
 * @property {string} wind_dir The wind direction as a string.
 * @property {number} pressure_mb The atmospheric pressure in millibars.
 * @property {number} pressure_in The atmospheric pressure in inches.
 * @property {number} precip_mm The precipitation amount in millimeters.
 * @property {number} precip_in The precipitation amount in inches.
 * @property {number} snow_cm The snowfall amount in centimeters.
 * @property {number} humidity The humidity percentage.
 * @property {number} cloud The cloud cover percentage.
 * @property {number} feelslike_c The feels-like temperature in Celsius.
 * @property {number} feelslike_f The feels-like temperature in Fahrenheit.
 * @property {number} windchill_c The wind chill temperature in Celsius.
 * @property {number} windchill_f The wind chill temperature in Fahrenheit.
 * @property {number} heatindex_c The heat index temperature in Celsius.
 * @property {number} heatindex_f The heat index temperature in Fahrenheit.
 * @property {number} dewpoint_c The dew point temperature in Celsius.
 * @property {number} dewpoint_f The dew point temperature in Fahrenheit.
 * @property {number} will_it_rain Indicator if it will rain (1) or not (0).
 * @property {number} chance_of_rain The chance of rain percentage.
 * @property {number} will_it_snow Indicator if it will snow (1) or not (0).
 * @property {number} chance_of_snow The chance of snow percentage.
 * @property {number} vis_km The visibility in kilometers.
 * @property {number} vis_miles The visibility in miles.
 * @property {number} gust_mph The wind gust speed in miles per hour.
 * @property {number} gust_kph The wind gust speed in kilometers per hour.
 * @property {number} uv The UV index.
 */

/**
 * @typedef {Object} Location
 * @property {string} name The name of the location.
 * @property {string} region The region of the location.
 * @property {string} country The country of the location.
 * @property {number} lat The latitude of the location.
 * @property {number} lon The longitude of the location.
 * @property {string} tz_id The timezone ID of the location.
 * @property {number} localtime_epoch The local time in epoch format.
 * @property {string} localtime The local time in human-readable format.
 */

/**
 * @typedef {Object} PrecipitationData
 * @property {number} rain Rainfall in mm
 * @property {number} snow Snowfall in cm
 */

/**
 * @typedef {Object} PrecipitationMemo
 * @property {PrecipitationData} h12 12-hour precipitation data
 * @property {PrecipitationData} h24 24-hour precipitation data
 */

/**
 * @typedef {Object} WeatherProviderProps
 * @property {string} key API key
 * @property {string} lat Location query
 */

/**
 * @typedef {Object} WeatherState
 * @property {Accessor<ForecastMemo>} forecast
 * @property {Accessor<PrecipitationMemo>} precipitation
 */

// ---

// Number of hourly weather forecasts to track
const MAX_FORECAST = 10

// Number of snowfall entries to track
const MAX_HISTORY = 24

// Key used to store weather history in local storage
// const KEY_STORAGE = 'today-weather-history'

const WeatherContext = createContext()

/**
 * @param {'forecast'|'history'} endpoint
 * @param {{ key: string, q: string, [param: string]: * }} request
 * @returns {Promise<T>}
 * @template {ForecastResponse|HistoryResponse} T
 */
const fetchJson = async (endpoint, request) => {
  const params = new URLSearchParams(request)
  const response = await fetch(`https://api.weatherapi.com/v1/${endpoint}.json?${params}`)

  if (!response.ok) throw new Error(`${response.status} fetching ${endpoint}`)

  return await response.json()
}

/**
 * @param {ForecastResponse} forecast
 * @returns {ForecastMemo[]}
 */
const parseForecast = (forecast) => {
  const results = forecast?.current ? [parseHour(forecast.current)] : []
  const time = isoDateTime(new Date(), 4).slice(0, -3)
  let length = results.length
  let push = false

  for (const day of forecast?.forecast.forecastday ?? []) {
    for (const hour of day.hour) {
      if (hour.time === time) push = true
      if (push) length = results.push(parseHour(hour))
      if (length >= MAX_FORECAST) break
    }
  }

  return results
}

/**
 * @param {ForecastResponse} forecast
 * @param {HistoryResponse} history
 * @returns {PrecipitationMemo}
 */
const parseHistory = (forecast, history) => {
  const data = []
  const results = {
    h12: { rain: 0, snow: 0 },
    h24: { rain: 0, snow: 0 }
  }
  const today = {
    time: isoDateTime(new Date(), 4).slice(0, -3),
    push: true
  }
  const yesterday = {
    time: isoDateTime((d) => d.setHours(d.getHours() - MAX_HISTORY), 4).slice(0, -3),
    push: false
  }

  for (const hour of history?.forecast.forecastday[0].hour ?? []) {
    if (hour.time === yesterday.time) yesterday.push = true
    if (yesterday.push) data.push(parseHour(hour))
  }

  for (const hour of forecast?.forecast.forecastday[0].hour ?? []) {
    if (today.push) data.push(parseHour(hour))
    if (hour.time === today.time) today.push = false
  }

  for (let i = 0; i < data.length; i++) {
    const { rain, snow } = data[i]

    if (i >= 12) {
      results.h12.rain += rain
      results.h12.snow += snow
    }

    results.h24.rain += rain
    results.h24.snow += snow

    if (i === data.length - 1) {
      results.h12.rain = Math.round(results.h12.rain)
      results.h12.snow = Math.round(results.h12.snow)
      results.h24.rain = Math.round(results.h24.rain)
      results.h24.snow = Math.round(results.h24.snow)
    }
  }

  return results
}

/**
 *
 * @param {Hour} hour
 * @returns {ForecastMemo}
 */
const parseHour = (hour = {}) => ({
  feel: Math.round(hour.feelslike_c),
  pop: hour.chance_of_rain,
  rain: Math.round(hour.precip_mm),
  snow: hour.snow_cm,
  temp: Math.round(hour.temp_c),
  text: hour.condition?.text,
  time: hour.time
})

// ---

/**
 * @param {ParentProps & WeatherProviderProps} props
 * @returns {JSXElement}
 */
export default function WeatherProvider (props) {
  const api = {
    key: props.key,
    q: props.query
  }

  const [{ hour }] = useSync()

  /** @type {ResourceReturn<ForecastResponse>} */
  const [weather] = createResource(() => hour(), async () => {
    // Get today and tomorrow's forecast
    const days = 2
    return await fetchJson('forecast', { ...api, days })
  })

  /** @type {ResourceReturn<HistoryResponse>} */
  const [history] = createResource(() => hour(), async () => {
    // Get yesterday's forecast
    const dt = isoDate((date) => date.setDate(date.getDate() - 1))
    return await fetchJson('history', { ...api, dt })
  })

  const forecast = createMemo(() => parseForecast(weather()))
  const precipitation = createMemo(() => parseHistory(history()))

  const state = {
    forecast,
    precipitation
  }

  return (
    <WeatherContext.Provider value={[state]}>
      {props.children}
    </WeatherContext.Provider>
  )
}

/**
 * @returns {[WeatherState]}
 */
export function useWeather () {
  return useContext(WeatherContext)
}
