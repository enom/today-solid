// Package imports
import { render } from 'solid-js/web'

// Application imports
import './index.css'
import App from './components/App.jsx'
import Boundary from './components/Boundary.jsx'
import Provider from './data/index.jsx'

// Mount and run application
render(() => (
  <Boundary>
    <Provider>
      <App />
    </Provider>
  </Boundary>
), document.getElementById('root'))
