import { Index } from 'solid-js'

export default function Repeat (props) {
  return (
    <Index each={new Array(props.count)}>
      {(_, i) => props.children(i)}
    </Index>
  )
}
