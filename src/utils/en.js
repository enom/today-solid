/**
 * Converts dates to locale date strings.
 *
 * @param {Date} date Date to stringify
 * @param {Object} options Locale
 * @returns {String}
 */
export function date (date, options) {
  return date.toLocaleDateString('en', options)
}

/**
 * Returns the suffix for day numbers.
 *
 * Getting the suffix for a number is a bit trickier than accessing an array index.
 *
 * @param {Number} num Number to suffix
 * @returns {String}
 */
export function suffix (num) {
  // Teens are always suffixed with "th" so 10th, 11th, 12th, 13th, etc.
  if (num >= 10 && num <= 20) return 'th'

  // 1st, 2nd, 3rd, 4th, 5th, 6th, 7th, etc.
  return ['th', 'st', 'nd', 'rd'][num % 10] || 'th'
}

/**
 * Converts dates to locale time strings.
 *
 * @param   {Date}   date Date to stringify
 * @param   {Object} options Locale options
 * @returns {String}
 */
export function time (date, options) {
  return date.toLocaleTimeString('en', options)
}
