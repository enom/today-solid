import now from './now.jsx'

/**
 * @typedef {import('./now.jsx').NowCallback} NowCallback
 */

/**
 * Generates an ISO 8601 formatted date and time string.
 *
 * The precision determines if a date/time component is reduced to 1/0 instead of the actual value.
 *
 * - 1: 2024-01-01 00:00:00
 * - 2: 2024-11-01 00:00:00
 * - 3: 2024-11-24 00:00:00
 * - 4: 2024-11-24 13:00:00
 * - 5: 2024-11-24 13:24:00
 * - 6: 2024-11-24 13:24:45
 *
 * @param {Date|NowCallback} [date=new Date()] The date object to format.
 * @param {number} [precision=6] The precision level for the time components
 * @returns {string}
 */
export default function isoDateTime (date = new Date(), precision = 6) {
  return `${isoDate(date)} ${isoTime(date, Math.min(0, precision - 3))}`
}

/**
 * Generates an ISO 8601 formatted time string.
 *
 * The precision determines if a time component is reduced to 0 instead of the actual value.
 *
 * - 1: 13:00:00
 * - 2: 13:24:00
 * - 3: 13:24:45
 *
 * @param {Date|NowCallback} [date=new Date()] The date object to format.
 * @param {number} [precision=3] The precision level for the time components.
 * @returns {string}
 */
export function isoTime (date = new Date(), precision = 3) {
  const { hh, ii, ss } = now(date)
  const p = (n, v) => precision >= n ? v : '00'
  return `${p(1, hh)}:${p(2, ii)}:${p(3, ss)}`
}

/**
 * Generates an ISO 8601 formatted date string.
 *
 * The precision determines if a date component is reduced to 1 instead of the actual value.
 *
 * - 1: 2024-01-01
 * - 2: 2024-11-01
 * - 3: 2024-11-24
 *
 * @param {Date|NowCallback} [date=new Date()] The date object to format.
 * @param {number} [precision=3] The precision level for the date components.
 * @returns {string}
 */
export function isoDate (date = new Date(), precision = 3) {
  if (precision < 1) throw new RangeError('isoDate() precision must be at least 1')
  const { yy, mm, dd } = now(date)
  const p = (n, v) => precision >= n ? v : '01'
  return `${yy}-${p(2, mm)}-${p(3, dd)}`
}
