import pad from '--/utils/pad.jsx'

/**
 * @typedef {Object} Now
 * @property {number} d The current day.
 * @property {number} dd The current padded day.
 * @property {number} h The current hour.
 * @property {number} hh The current padded hour.
 * @property {number} i The current minute.
 * @property {number} ii The current padded minute.
 * @property {number} m The current month.
 * @property {number} mm The current padded month.
 * @property {number} s The current second.
 * @property {number} ss The current padded second.
 * @property {number} y The current two digit year.
 * @property {number} yy The current full year.
 */

/**
 * @typedef {(date: Date) => void} NowCallback
 */

/**
 * Returns the current date and time as an object with year, month, day, hours,
 * minutes, and seconds.
 *
 * @param {Date|NowCallback} [init] Date to get the current time
 * @returns {Now}
 */
export default function now (init) {
  const date = typeof init === 'function'
    ? new Date()
    : init !== undefined
      ? new Date(init)
      : new Date()

  if (typeof init === 'function') init(date)

  const yy = date.getFullYear()
  const m = date.getMonth() + 1 // Months are zero-based, so we add 1
  const d = date.getDate()
  const h = date.getHours()
  const i = date.getMinutes()
  const s = date.getSeconds()

  return {
    d,
    dd: pad(d),
    h,
    hh: pad(h),
    i,
    ii: pad(i),
    m,
    mm: pad(m),
    s,
    ss: pad(s),
    y: yy - 2000,
    yy
  }
};
