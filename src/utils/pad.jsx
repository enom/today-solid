/**
 * Pads a given number with a leading 0.
 *
 * @param {number} n Number to pad
 * @returns {string}
 */
export default function pad (n) {
  return n < 10 ? `0${n}` : n
}
